/*
    Simple HTTP proxy in Rust. Hard coded to proxy rust-lang.org.
*/

extern crate hyper;

use std::io::Read;

use hyper::Client;
use hyper::header::Connection;

use hyper::Server;
use hyper::server::Request;
use hyper::server::Response;

use hyper::uri::RequestUri;

fn proxy(proxy_request: Request, proxy_response: Response) {
    let client = Client::new();

    match (proxy_request.method, proxy_request.uri) {
        (hyper::Get, RequestUri::AbsolutePath(ref path)) => {
            let uri = "http://rust-lang.org".to_string() + &path;
            let mut response = client.get(&uri)
                .header(Connection::close())
                .send().unwrap();

            let mut body = String::new();
            response.read_to_string(&mut body).unwrap();

            proxy_response.send(&body.into_bytes()).unwrap();
        },
        _ => {}
    }
}

fn main() {
    Server::http("127.0.0.1:3000").unwrap().handle(proxy);
}